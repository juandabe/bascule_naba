/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siscafe.bascule;

import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author jeiso
 */
@Path("WeightNB")
public class WeightResource {

    @EJB
    BasculeManager scaleManager;

    /**
     * Creates a new instance of WSResource
     */
    public WeightResource() {
    }

    /**
     * Retrieves representation of an instance of com.siscafe.bascule.WSResource
     * @return an instance of java.lang.String
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Path("getWeightCom1")
    public String getWeightCom1() {
        return String.valueOf(scaleManager.getWeight1());
    }
    
    /**
     * Retrieves representation of an instance of com.siscafe.bascule.WSResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getWeightCom2")
    public String getWeightCom2() {
        return String.valueOf(scaleManager.getWeight2());
    }

}
