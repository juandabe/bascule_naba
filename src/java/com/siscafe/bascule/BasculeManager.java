/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siscafe.bascule;

import com.fazecast.jSerialComm.SerialPort;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author jeiso
 */
@Singleton
@Startup
@ConcurrencyManagement(BEAN)
public class BasculeManager {

    private String weight1;
    private String weight2;
    private Thread reader;

    @PostConstruct
    public void applicationStartup() {
        //reader = new Thread(new ScaleReader());
        //reader.start();
        connect1();
        connect2();
    }

    public String getWeight1() {
        return weight1;
    }
    
    public String getWeight2() {
        return weight2;
    }

    public void connect1() {
        SerialPort comPort = SerialPort.getCommPort("COM1");
        comPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
        comPort.setBaudRate(1200);
        comPort.setParity(SerialPort.EVEN_PARITY);
        comPort.setNumStopBits(SerialPort.TWO_STOP_BITS);
        comPort.setNumDataBits(7);
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 500, 0);
        comPort.openPort();
        (new Thread(new SerialReader1(comPort))).start();
    }
    
    public void connect2() {
        SerialPort comPort = SerialPort.getCommPort("COM4");
        comPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
        comPort.setBaudRate(9600);
        comPort.setParity(SerialPort.NO_PARITY);
        comPort.setNumStopBits(SerialPort.ONE_STOP_BIT);
        comPort.setNumDataBits(8);
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 500, 0);
        comPort.openPort();
        (new Thread(new SerialReader2(comPort))).start();
    }

    private void capturingWeigh1(SerialPort comPort) {
        String line="";
        BufferedReader reader = new BufferedReader(new InputStreamReader(comPort.getInputStream()));
        while(true){
            try {
                while ((reader.ready()) && (line = reader.readLine()) != null) {
                    getWieghtSpb1(line);
                    //System.out.println(line + " - " +line.length());
                }
            } catch (IOException ex) {
                Logger.getLogger(BasculeManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void getWieghtSpb1(String readLine) {
        if(readLine.length()== 16){
            weight1 = readLine.substring(6, 10);
            //weight1 = readLine.substring(6, 14);
            //System.out.println(weight);
        }
        else{
            weight1 = "";
        }
    }
    
    private void capturingWeigh2(SerialPort comPort) {
        String line="";
        BufferedReader reader = new BufferedReader(new InputStreamReader(comPort.getInputStream()));
        while(true){
            try {
                while ((reader.ready()) && (line = reader.readLine()) != null) {
                    getWieghtSpb2(line);
                    //System.out.println(line + " - " +line.length());
                }
            } catch (IOException ex) {
                Logger.getLogger(BasculeManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void getWieghtSpb2(String readLine) {
        if(readLine.length()== 16){
            weight2 = readLine.substring(6, 14);
            //weight2 = readLine.substring(5, 10);
            //System.out.println(weight);
        }
        else{
            weight2 = "";
        }
    }

    /**
     *
     */
    class SerialReader1 implements Runnable 
    {
        SerialPort comPort1;
        
        public SerialReader1 ( SerialPort comPort1 )
        {
            this.comPort1 = comPort1;
        }
        
        public void run ()
        {
                capturingWeigh1(comPort1);
        }
    }
    
    class SerialReader2 implements Runnable 
    {
        SerialPort comPort2;
        
        public SerialReader2 ( SerialPort comPort2 )
        {
            this.comPort2 = comPort2;
        }
        
        public void run ()
        {
                capturingWeigh2(comPort2);
        }
    }

}
